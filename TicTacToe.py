import os

# Create and return an empty board
class Game:
    # To-Do after base game is working: Work a method to make it so when changing the size it also changed conditions of the game (win conditions, board size etc)
    size = 3 # Size of x and y of game board
    match = False # True if game is completed, False otherwise

    # On creation of object
    def __init__(self, turn):
        self.emptyBoard()
        
        self.turn = turn # Will only ever be X or O

    # Sets the board to be empty
    def emptyBoard(self):
        self.board = [[" ", " ", " "], [" ", " ", " "], [" ", " ", " "]]

    # Counter is the player
    def move(self, x, y):
        self.board[x][y] = game.turn

        self.checkGameState()

        if self.match == False:
            self.switchTurn()

    # To change alternate the turn of the counter
    def switchTurn(self):
        if self.turn == "X": 
            self.turn = "O"
        else: 
            self.turn = "X"

    # Call the board with an x and y value to return the value in that location
    def returnValue(self, x, y):
        return self.board[x][y]

    # Prints the current board
    def printBoard(self):
        print("   1 2 3")
        
        for i in range(game.size):
            print("  -------")
            print(i + 1, "|", end='')

            for j in range(game.size):
                print(game.board[i][j], end='')
                print("|", end='')

            print("")
        
        print("  -------")

    # Checks state of game, if still going then no change occurs, if game completed then 
    def checkGameState(self):
        pass

def clearTerminal():
    # Clears terminal
    os.system('cls' if os.name == 'nt' else 'clear') # cls command on Windows, or clear on Unix systems

def runGame():
    # Call the global instance of game
    global game
    
    while not game.match:
        # Clear terminal
        clearTerminal()

        # Print the current board state
        game.printBoard()

        # Display who's turn it is
        print("It is", game.turn, "turn to go\n")

        while True:
            # Prompting user to make move
            x = input("Enter X value: ")
            y = input("Enter Y value: ")
            #print("\nX: ", x, "\nY: ", y, "\nValue in array: ", game.returnValue(int(y) - 1, int(x) - 1))

            # Validate move: move must not be occupied, must be within board
            if game.returnValue(int(y) - 1, int(x) - 1) == " ":
                break

            print("\nERROR: Location specified must have an unoccupied space\n")

        # Place the counter in the prompted location
        game.move(int(y) - 1, int(x) - 1)

        # Check game completion

        # Loop back to beggining of loop if game is not completed

    # After breaking out of loop run the appropriate congratulatory message for winner using game.turn to find winner

    # Prompt user to continue playing

def start():
    global game

    # Title and author
    print("Welcome to TicTacToe\nCreated by Shuaib Shahbaz\n")

    input("Press enter to start")
    print("")

    clearTerminal()

    # Selecting who goes first
    while True:
        # Prmpting user input and setting user input to be upper case
        first = input("Who goes first, X or O: ").upper()

        # Checking if input is valid
        if first == "X" or first == "O": 
            game = Game(first) # Create new game instance
            break # Break out of while loop
        else:
            # Print error message
            print("\nERROR: Invalid input! Please enter only X or O\n")

    runGame()


start()